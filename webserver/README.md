## Running on Windows
When running on Windows 10 Fall 2017 Creators Update (or later), Python 3.6+ is required [due to a Python bug](https://bugs.python.org/issue32245).